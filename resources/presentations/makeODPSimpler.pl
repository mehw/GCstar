#!/usr/bin/perl

use strict;

use OpenOffice::OODoc;
use File::Copy;

copy 'PresentationJM2L.odp', 'PresentationJM2L_light.odp';

# get global access to the content of an OOo file
my $document = odfDocument(file => 'PresentationJM2L_light.odp');

for my $par ($document->getElementList('//anim:par'))
{
    my $class = $document->getAttribute($par, 'presentation:preset-class');
    next if !$class;
    $document->setAttributes($par, 'presentation:preset-id' => 'ooo-entrance-appear')
        if $class eq 'entrance';
    $document->setAttributes($par, 'presentation:preset-id' => 'ooo-exit-disappear')
        if $class eq 'exit';
}
 
for my $animation ($document->getElementList('//anim:animate'))
{
    $document->removeElement($animation);
}

for my $animation ($document->getElementList('//anim:set'))
{
    $document->setAttributes($animation, 'smil:begin' => '0s');
}


$document->createStyle(
                       'SimpleTransition',
                       family => 'drawing-page',
                       properties => {}
                      );

for my $page ($document->getElementList('//draw:page'))
{
    $document->setAttributes($page, 'draw:style-name' => 'SimpleTransition');
}

for my $filter ($document->getElementList('//anim:transitionFilter'))
{
    $document->removeElement($filter);
}

$document->save;

